
package cr.ac.una.gestorseguridad.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para usuarioDto complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="usuarioDto"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="modificado" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="roles" type="{http://controller.gestorseguridadws.una.ac.cr/}rolDto" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuAdministrador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuCedula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuCelular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuClave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuClaveTemp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuCorreo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuFoto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuIdioma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuPapellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuSapellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "usuarioDto", propOrder = {
    "modificado",
    "roles",
    "token",
    "usuAdministrador",
    "usuCedula",
    "usuCelular",
    "usuClave",
    "usuClaveTemp",
    "usuCorreo",
    "usuEstado",
    "usuFoto",
    "usuId",
    "usuIdioma",
    "usuNombre",
    "usuPapellido",
    "usuSapellido",
    "usuTelefono",
    "usuUsuario"
})
public class UsuarioDto {

    protected Boolean modificado;
    @XmlElement(nillable = true)
    protected List<RolDto> roles;
    protected String token;
    protected String usuAdministrador;
    protected String usuCedula;
    protected String usuCelular;
    protected String usuClave;
    protected String usuClaveTemp;
    protected String usuCorreo;
    protected String usuEstado;
    protected byte[] usuFoto;
    protected Long usuId;
    protected String usuIdioma;
    protected String usuNombre;
    protected String usuPapellido;
    protected String usuSapellido;
    protected String usuTelefono;
    protected String usuUsuario;

    /**
     * Obtiene el valor de la propiedad modificado.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isModificado() {
        return modificado;
    }

    /**
     * Define el valor de la propiedad modificado.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setModificado(Boolean value) {
        this.modificado = value;
    }

    /**
     * Gets the value of the roles property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roles property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRoles().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RolDto }
     * 
     * 
     */
    public List<RolDto> getRoles() {
        if (roles == null) {
            roles = new ArrayList<RolDto>();
        }
        return this.roles;
    }

    /**
     * Obtiene el valor de la propiedad token.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Obtiene el valor de la propiedad usuAdministrador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuAdministrador() {
        return usuAdministrador;
    }

    /**
     * Define el valor de la propiedad usuAdministrador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuAdministrador(String value) {
        this.usuAdministrador = value;
    }

    /**
     * Obtiene el valor de la propiedad usuCedula.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuCedula() {
        return usuCedula;
    }

    /**
     * Define el valor de la propiedad usuCedula.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuCedula(String value) {
        this.usuCedula = value;
    }

    /**
     * Obtiene el valor de la propiedad usuCelular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuCelular() {
        return usuCelular;
    }

    /**
     * Define el valor de la propiedad usuCelular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuCelular(String value) {
        this.usuCelular = value;
    }

    /**
     * Obtiene el valor de la propiedad usuClave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuClave() {
        return usuClave;
    }

    /**
     * Define el valor de la propiedad usuClave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuClave(String value) {
        this.usuClave = value;
    }

    /**
     * Obtiene el valor de la propiedad usuClaveTemp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuClaveTemp() {
        return usuClaveTemp;
    }

    /**
     * Define el valor de la propiedad usuClaveTemp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuClaveTemp(String value) {
        this.usuClaveTemp = value;
    }

    /**
     * Obtiene el valor de la propiedad usuCorreo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuCorreo() {
        return usuCorreo;
    }

    /**
     * Define el valor de la propiedad usuCorreo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuCorreo(String value) {
        this.usuCorreo = value;
    }

    /**
     * Obtiene el valor de la propiedad usuEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuEstado() {
        return usuEstado;
    }

    /**
     * Define el valor de la propiedad usuEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuEstado(String value) {
        this.usuEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad usuFoto.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getUsuFoto() {
        return usuFoto;
    }

    /**
     * Define el valor de la propiedad usuFoto.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setUsuFoto(byte[] value) {
        this.usuFoto = value;
    }

    /**
     * Obtiene el valor de la propiedad usuId.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUsuId() {
        return usuId;
    }

    /**
     * Define el valor de la propiedad usuId.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUsuId(Long value) {
        this.usuId = value;
    }

    /**
     * Obtiene el valor de la propiedad usuIdioma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuIdioma() {
        return usuIdioma;
    }

    /**
     * Define el valor de la propiedad usuIdioma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuIdioma(String value) {
        this.usuIdioma = value;
    }

    /**
     * Obtiene el valor de la propiedad usuNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuNombre() {
        return usuNombre;
    }

    /**
     * Define el valor de la propiedad usuNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuNombre(String value) {
        this.usuNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad usuPapellido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuPapellido() {
        return usuPapellido;
    }

    /**
     * Define el valor de la propiedad usuPapellido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuPapellido(String value) {
        this.usuPapellido = value;
    }

    /**
     * Obtiene el valor de la propiedad usuSapellido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuSapellido() {
        return usuSapellido;
    }

    /**
     * Define el valor de la propiedad usuSapellido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuSapellido(String value) {
        this.usuSapellido = value;
    }

    /**
     * Obtiene el valor de la propiedad usuTelefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuTelefono() {
        return usuTelefono;
    }

    /**
     * Define el valor de la propiedad usuTelefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuTelefono(String value) {
        this.usuTelefono = value;
    }

    /**
     * Obtiene el valor de la propiedad usuUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuUsuario() {
        return usuUsuario;
    }

    /**
     * Define el valor de la propiedad usuUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuUsuario(String value) {
        this.usuUsuario = value;
    }

}
