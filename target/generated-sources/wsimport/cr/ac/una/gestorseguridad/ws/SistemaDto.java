
package cr.ac.una.gestorseguridad.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para sistemaDto complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="sistemaDto"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="modificado" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="sisEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="sisId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="sisNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sistemaDto", propOrder = {
    "modificado",
    "sisEstado",
    "sisId",
    "sisNombre"
})
public class SistemaDto {

    protected Boolean modificado;
    protected String sisEstado;
    protected Long sisId;
    protected String sisNombre;

    /**
     * Obtiene el valor de la propiedad modificado.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isModificado() {
        return modificado;
    }

    /**
     * Define el valor de la propiedad modificado.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setModificado(Boolean value) {
        this.modificado = value;
    }

    /**
     * Obtiene el valor de la propiedad sisEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSisEstado() {
        return sisEstado;
    }

    /**
     * Define el valor de la propiedad sisEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSisEstado(String value) {
        this.sisEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad sisId.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSisId() {
        return sisId;
    }

    /**
     * Define el valor de la propiedad sisId.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSisId(Long value) {
        this.sisId = value;
    }

    /**
     * Obtiene el valor de la propiedad sisNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSisNombre() {
        return sisNombre;
    }

    /**
     * Define el valor de la propiedad sisNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSisNombre(String value) {
        this.sisNombre = value;
    }

}
