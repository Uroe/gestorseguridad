
package cr.ac.una.gestorseguridad.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para rolDto complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="rolDto"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="rolEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="rolId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="rolNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="sistema" type="{http://controller.gestorseguridadws.una.ac.cr/}sistemaDto" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuarios" type="{http://controller.gestorseguridadws.una.ac.cr/}usuarioDto" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="usuariosEliminados" type="{http://controller.gestorseguridadws.una.ac.cr/}usuarioDto" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rolDto", propOrder = {
    "rolEstado",
    "rolId",
    "rolNombre",
    "sistema",
    "usuarios",
    "usuariosEliminados"
})
public class RolDto {

    protected String rolEstado;
    protected Long rolId;
    protected String rolNombre;
    protected SistemaDto sistema;
    @XmlElement(nillable = true)
    protected List<UsuarioDto> usuarios;
    @XmlElement(nillable = true)
    protected List<UsuarioDto> usuariosEliminados;

    /**
     * Obtiene el valor de la propiedad rolEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolEstado() {
        return rolEstado;
    }

    /**
     * Define el valor de la propiedad rolEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolEstado(String value) {
        this.rolEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad rolId.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRolId() {
        return rolId;
    }

    /**
     * Define el valor de la propiedad rolId.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRolId(Long value) {
        this.rolId = value;
    }

    /**
     * Obtiene el valor de la propiedad rolNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolNombre() {
        return rolNombre;
    }

    /**
     * Define el valor de la propiedad rolNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolNombre(String value) {
        this.rolNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad sistema.
     * 
     * @return
     *     possible object is
     *     {@link SistemaDto }
     *     
     */
    public SistemaDto getSistema() {
        return sistema;
    }

    /**
     * Define el valor de la propiedad sistema.
     * 
     * @param value
     *     allowed object is
     *     {@link SistemaDto }
     *     
     */
    public void setSistema(SistemaDto value) {
        this.sistema = value;
    }

    /**
     * Gets the value of the usuarios property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the usuarios property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getUsuarios().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link UsuarioDto }
     * 
     * 
     */
    public List<UsuarioDto> getUsuarios() {
        if (usuarios == null) {
            usuarios = new ArrayList<UsuarioDto>();
        }
        return this.usuarios;
    }

    /**
     * Gets the value of the usuariosEliminados property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the usuariosEliminados property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getUsuariosEliminados().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link UsuarioDto }
     * 
     * 
     */
    public List<UsuarioDto> getUsuariosEliminados() {
        if (usuariosEliminados == null) {
            usuariosEliminados = new ArrayList<UsuarioDto>();
        }
        return this.usuariosEliminados;
    }

}
