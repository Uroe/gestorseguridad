
package cr.ac.una.gestorseguridad.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cr.ac.una.gestorseguridad.ws package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ActivarUsuario_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "activarUsuario");
    private final static QName _ActivarUsuarioResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "activarUsuarioResponse");
    private final static QName _AsignarAdministrador_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "asignarAdministrador");
    private final static QName _AsignarAdministradorResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "asignarAdministradorResponse");
    private final static QName _CambiarClave_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "cambiarClave");
    private final static QName _CambiarClaveResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "cambiarClaveResponse");
    private final static QName _EliminarRol_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "eliminarRol");
    private final static QName _EliminarRolResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "eliminarRolResponse");
    private final static QName _EliminarSistema_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "eliminarSistema");
    private final static QName _EliminarSistemaResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "eliminarSistemaResponse");
    private final static QName _EliminarUsuario_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "eliminarUsuario");
    private final static QName _EliminarUsuarioResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "eliminarUsuarioResponse");
    private final static QName _GenerarClaveTemp_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "generarClaveTemp");
    private final static QName _GenerarClaveTempResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "generarClaveTempResponse");
    private final static QName _GetRol_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getRol");
    private final static QName _GetRolResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getRolResponse");
    private final static QName _GetRoles_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getRoles");
    private final static QName _GetRolesResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getRolesResponse");
    private final static QName _GetSistema_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getSistema");
    private final static QName _GetSistemaResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getSistemaResponse");
    private final static QName _GetSistemas_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getSistemas");
    private final static QName _GetSistemasResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getSistemasResponse");
    private final static QName _GetUsuario_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getUsuario");
    private final static QName _GetUsuarioResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getUsuarioResponse");
    private final static QName _GetUsuarios_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getUsuarios");
    private final static QName _GetUsuariosResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "getUsuariosResponse");
    private final static QName _GuardarRol_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "guardarRol");
    private final static QName _GuardarRolResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "guardarRolResponse");
    private final static QName _GuardarSistema_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "guardarSistema");
    private final static QName _GuardarSistemaResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "guardarSistemaResponse");
    private final static QName _GuardarUsuario_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "guardarUsuario");
    private final static QName _GuardarUsuarioResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "guardarUsuarioResponse");
    private final static QName _ValidarUsuario_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "validarUsuario");
    private final static QName _ValidarUsuarioResponse_QNAME = new QName("http://controller.gestorseguridadws.una.ac.cr/", "validarUsuarioResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cr.ac.una.gestorseguridad.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActivarUsuario }
     * 
     */
    public ActivarUsuario createActivarUsuario() {
        return new ActivarUsuario();
    }

    /**
     * Create an instance of {@link ActivarUsuarioResponse }
     * 
     */
    public ActivarUsuarioResponse createActivarUsuarioResponse() {
        return new ActivarUsuarioResponse();
    }

    /**
     * Create an instance of {@link AsignarAdministrador }
     * 
     */
    public AsignarAdministrador createAsignarAdministrador() {
        return new AsignarAdministrador();
    }

    /**
     * Create an instance of {@link AsignarAdministradorResponse }
     * 
     */
    public AsignarAdministradorResponse createAsignarAdministradorResponse() {
        return new AsignarAdministradorResponse();
    }

    /**
     * Create an instance of {@link CambiarClave }
     * 
     */
    public CambiarClave createCambiarClave() {
        return new CambiarClave();
    }

    /**
     * Create an instance of {@link CambiarClaveResponse }
     * 
     */
    public CambiarClaveResponse createCambiarClaveResponse() {
        return new CambiarClaveResponse();
    }

    /**
     * Create an instance of {@link EliminarRol }
     * 
     */
    public EliminarRol createEliminarRol() {
        return new EliminarRol();
    }

    /**
     * Create an instance of {@link EliminarRolResponse }
     * 
     */
    public EliminarRolResponse createEliminarRolResponse() {
        return new EliminarRolResponse();
    }

    /**
     * Create an instance of {@link EliminarSistema }
     * 
     */
    public EliminarSistema createEliminarSistema() {
        return new EliminarSistema();
    }

    /**
     * Create an instance of {@link EliminarSistemaResponse }
     * 
     */
    public EliminarSistemaResponse createEliminarSistemaResponse() {
        return new EliminarSistemaResponse();
    }

    /**
     * Create an instance of {@link EliminarUsuario }
     * 
     */
    public EliminarUsuario createEliminarUsuario() {
        return new EliminarUsuario();
    }

    /**
     * Create an instance of {@link EliminarUsuarioResponse }
     * 
     */
    public EliminarUsuarioResponse createEliminarUsuarioResponse() {
        return new EliminarUsuarioResponse();
    }

    /**
     * Create an instance of {@link GenerarClaveTemp }
     * 
     */
    public GenerarClaveTemp createGenerarClaveTemp() {
        return new GenerarClaveTemp();
    }

    /**
     * Create an instance of {@link GenerarClaveTempResponse }
     * 
     */
    public GenerarClaveTempResponse createGenerarClaveTempResponse() {
        return new GenerarClaveTempResponse();
    }

    /**
     * Create an instance of {@link GetRol }
     * 
     */
    public GetRol createGetRol() {
        return new GetRol();
    }

    /**
     * Create an instance of {@link GetRolResponse }
     * 
     */
    public GetRolResponse createGetRolResponse() {
        return new GetRolResponse();
    }

    /**
     * Create an instance of {@link GetRoles }
     * 
     */
    public GetRoles createGetRoles() {
        return new GetRoles();
    }

    /**
     * Create an instance of {@link GetRolesResponse }
     * 
     */
    public GetRolesResponse createGetRolesResponse() {
        return new GetRolesResponse();
    }

    /**
     * Create an instance of {@link GetSistema }
     * 
     */
    public GetSistema createGetSistema() {
        return new GetSistema();
    }

    /**
     * Create an instance of {@link GetSistemaResponse }
     * 
     */
    public GetSistemaResponse createGetSistemaResponse() {
        return new GetSistemaResponse();
    }

    /**
     * Create an instance of {@link GetSistemas }
     * 
     */
    public GetSistemas createGetSistemas() {
        return new GetSistemas();
    }

    /**
     * Create an instance of {@link GetSistemasResponse }
     * 
     */
    public GetSistemasResponse createGetSistemasResponse() {
        return new GetSistemasResponse();
    }

    /**
     * Create an instance of {@link GetUsuario }
     * 
     */
    public GetUsuario createGetUsuario() {
        return new GetUsuario();
    }

    /**
     * Create an instance of {@link GetUsuarioResponse }
     * 
     */
    public GetUsuarioResponse createGetUsuarioResponse() {
        return new GetUsuarioResponse();
    }

    /**
     * Create an instance of {@link GetUsuarios }
     * 
     */
    public GetUsuarios createGetUsuarios() {
        return new GetUsuarios();
    }

    /**
     * Create an instance of {@link GetUsuariosResponse }
     * 
     */
    public GetUsuariosResponse createGetUsuariosResponse() {
        return new GetUsuariosResponse();
    }

    /**
     * Create an instance of {@link GuardarRol }
     * 
     */
    public GuardarRol createGuardarRol() {
        return new GuardarRol();
    }

    /**
     * Create an instance of {@link GuardarRolResponse }
     * 
     */
    public GuardarRolResponse createGuardarRolResponse() {
        return new GuardarRolResponse();
    }

    /**
     * Create an instance of {@link GuardarSistema }
     * 
     */
    public GuardarSistema createGuardarSistema() {
        return new GuardarSistema();
    }

    /**
     * Create an instance of {@link GuardarSistemaResponse }
     * 
     */
    public GuardarSistemaResponse createGuardarSistemaResponse() {
        return new GuardarSistemaResponse();
    }

    /**
     * Create an instance of {@link GuardarUsuario }
     * 
     */
    public GuardarUsuario createGuardarUsuario() {
        return new GuardarUsuario();
    }

    /**
     * Create an instance of {@link GuardarUsuarioResponse }
     * 
     */
    public GuardarUsuarioResponse createGuardarUsuarioResponse() {
        return new GuardarUsuarioResponse();
    }

    /**
     * Create an instance of {@link ValidarUsuario }
     * 
     */
    public ValidarUsuario createValidarUsuario() {
        return new ValidarUsuario();
    }

    /**
     * Create an instance of {@link ValidarUsuarioResponse }
     * 
     */
    public ValidarUsuarioResponse createValidarUsuarioResponse() {
        return new ValidarUsuarioResponse();
    }

    /**
     * Create an instance of {@link UsuarioDto }
     * 
     */
    public UsuarioDto createUsuarioDto() {
        return new UsuarioDto();
    }

    /**
     * Create an instance of {@link RolDto }
     * 
     */
    public RolDto createRolDto() {
        return new RolDto();
    }

    /**
     * Create an instance of {@link SistemaDto }
     * 
     */
    public SistemaDto createSistemaDto() {
        return new SistemaDto();
    }

    /**
     * Create an instance of {@link Respuesta }
     * 
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivarUsuario }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ActivarUsuario }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "activarUsuario")
    public JAXBElement<ActivarUsuario> createActivarUsuario(ActivarUsuario value) {
        return new JAXBElement<ActivarUsuario>(_ActivarUsuario_QNAME, ActivarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivarUsuarioResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ActivarUsuarioResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "activarUsuarioResponse")
    public JAXBElement<ActivarUsuarioResponse> createActivarUsuarioResponse(ActivarUsuarioResponse value) {
        return new JAXBElement<ActivarUsuarioResponse>(_ActivarUsuarioResponse_QNAME, ActivarUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignarAdministrador }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AsignarAdministrador }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "asignarAdministrador")
    public JAXBElement<AsignarAdministrador> createAsignarAdministrador(AsignarAdministrador value) {
        return new JAXBElement<AsignarAdministrador>(_AsignarAdministrador_QNAME, AsignarAdministrador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignarAdministradorResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AsignarAdministradorResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "asignarAdministradorResponse")
    public JAXBElement<AsignarAdministradorResponse> createAsignarAdministradorResponse(AsignarAdministradorResponse value) {
        return new JAXBElement<AsignarAdministradorResponse>(_AsignarAdministradorResponse_QNAME, AsignarAdministradorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CambiarClave }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CambiarClave }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "cambiarClave")
    public JAXBElement<CambiarClave> createCambiarClave(CambiarClave value) {
        return new JAXBElement<CambiarClave>(_CambiarClave_QNAME, CambiarClave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CambiarClaveResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CambiarClaveResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "cambiarClaveResponse")
    public JAXBElement<CambiarClaveResponse> createCambiarClaveResponse(CambiarClaveResponse value) {
        return new JAXBElement<CambiarClaveResponse>(_CambiarClaveResponse_QNAME, CambiarClaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarRol }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EliminarRol }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "eliminarRol")
    public JAXBElement<EliminarRol> createEliminarRol(EliminarRol value) {
        return new JAXBElement<EliminarRol>(_EliminarRol_QNAME, EliminarRol.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarRolResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EliminarRolResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "eliminarRolResponse")
    public JAXBElement<EliminarRolResponse> createEliminarRolResponse(EliminarRolResponse value) {
        return new JAXBElement<EliminarRolResponse>(_EliminarRolResponse_QNAME, EliminarRolResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarSistema }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EliminarSistema }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "eliminarSistema")
    public JAXBElement<EliminarSistema> createEliminarSistema(EliminarSistema value) {
        return new JAXBElement<EliminarSistema>(_EliminarSistema_QNAME, EliminarSistema.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarSistemaResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EliminarSistemaResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "eliminarSistemaResponse")
    public JAXBElement<EliminarSistemaResponse> createEliminarSistemaResponse(EliminarSistemaResponse value) {
        return new JAXBElement<EliminarSistemaResponse>(_EliminarSistemaResponse_QNAME, EliminarSistemaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarUsuario }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EliminarUsuario }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "eliminarUsuario")
    public JAXBElement<EliminarUsuario> createEliminarUsuario(EliminarUsuario value) {
        return new JAXBElement<EliminarUsuario>(_EliminarUsuario_QNAME, EliminarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarUsuarioResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EliminarUsuarioResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "eliminarUsuarioResponse")
    public JAXBElement<EliminarUsuarioResponse> createEliminarUsuarioResponse(EliminarUsuarioResponse value) {
        return new JAXBElement<EliminarUsuarioResponse>(_EliminarUsuarioResponse_QNAME, EliminarUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarClaveTemp }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GenerarClaveTemp }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "generarClaveTemp")
    public JAXBElement<GenerarClaveTemp> createGenerarClaveTemp(GenerarClaveTemp value) {
        return new JAXBElement<GenerarClaveTemp>(_GenerarClaveTemp_QNAME, GenerarClaveTemp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarClaveTempResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GenerarClaveTempResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "generarClaveTempResponse")
    public JAXBElement<GenerarClaveTempResponse> createGenerarClaveTempResponse(GenerarClaveTempResponse value) {
        return new JAXBElement<GenerarClaveTempResponse>(_GenerarClaveTempResponse_QNAME, GenerarClaveTempResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRol }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetRol }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getRol")
    public JAXBElement<GetRol> createGetRol(GetRol value) {
        return new JAXBElement<GetRol>(_GetRol_QNAME, GetRol.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRolResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetRolResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getRolResponse")
    public JAXBElement<GetRolResponse> createGetRolResponse(GetRolResponse value) {
        return new JAXBElement<GetRolResponse>(_GetRolResponse_QNAME, GetRolResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRoles }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetRoles }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getRoles")
    public JAXBElement<GetRoles> createGetRoles(GetRoles value) {
        return new JAXBElement<GetRoles>(_GetRoles_QNAME, GetRoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRolesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetRolesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getRolesResponse")
    public JAXBElement<GetRolesResponse> createGetRolesResponse(GetRolesResponse value) {
        return new JAXBElement<GetRolesResponse>(_GetRolesResponse_QNAME, GetRolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSistema }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetSistema }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getSistema")
    public JAXBElement<GetSistema> createGetSistema(GetSistema value) {
        return new JAXBElement<GetSistema>(_GetSistema_QNAME, GetSistema.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSistemaResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetSistemaResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getSistemaResponse")
    public JAXBElement<GetSistemaResponse> createGetSistemaResponse(GetSistemaResponse value) {
        return new JAXBElement<GetSistemaResponse>(_GetSistemaResponse_QNAME, GetSistemaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSistemas }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetSistemas }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getSistemas")
    public JAXBElement<GetSistemas> createGetSistemas(GetSistemas value) {
        return new JAXBElement<GetSistemas>(_GetSistemas_QNAME, GetSistemas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSistemasResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetSistemasResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getSistemasResponse")
    public JAXBElement<GetSistemasResponse> createGetSistemasResponse(GetSistemasResponse value) {
        return new JAXBElement<GetSistemasResponse>(_GetSistemasResponse_QNAME, GetSistemasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsuario }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUsuario }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getUsuario")
    public JAXBElement<GetUsuario> createGetUsuario(GetUsuario value) {
        return new JAXBElement<GetUsuario>(_GetUsuario_QNAME, GetUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsuarioResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUsuarioResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getUsuarioResponse")
    public JAXBElement<GetUsuarioResponse> createGetUsuarioResponse(GetUsuarioResponse value) {
        return new JAXBElement<GetUsuarioResponse>(_GetUsuarioResponse_QNAME, GetUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsuarios }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUsuarios }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getUsuarios")
    public JAXBElement<GetUsuarios> createGetUsuarios(GetUsuarios value) {
        return new JAXBElement<GetUsuarios>(_GetUsuarios_QNAME, GetUsuarios.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsuariosResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUsuariosResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "getUsuariosResponse")
    public JAXBElement<GetUsuariosResponse> createGetUsuariosResponse(GetUsuariosResponse value) {
        return new JAXBElement<GetUsuariosResponse>(_GetUsuariosResponse_QNAME, GetUsuariosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarRol }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GuardarRol }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "guardarRol")
    public JAXBElement<GuardarRol> createGuardarRol(GuardarRol value) {
        return new JAXBElement<GuardarRol>(_GuardarRol_QNAME, GuardarRol.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarRolResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GuardarRolResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "guardarRolResponse")
    public JAXBElement<GuardarRolResponse> createGuardarRolResponse(GuardarRolResponse value) {
        return new JAXBElement<GuardarRolResponse>(_GuardarRolResponse_QNAME, GuardarRolResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarSistema }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GuardarSistema }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "guardarSistema")
    public JAXBElement<GuardarSistema> createGuardarSistema(GuardarSistema value) {
        return new JAXBElement<GuardarSistema>(_GuardarSistema_QNAME, GuardarSistema.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarSistemaResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GuardarSistemaResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "guardarSistemaResponse")
    public JAXBElement<GuardarSistemaResponse> createGuardarSistemaResponse(GuardarSistemaResponse value) {
        return new JAXBElement<GuardarSistemaResponse>(_GuardarSistemaResponse_QNAME, GuardarSistemaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarUsuario }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GuardarUsuario }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "guardarUsuario")
    public JAXBElement<GuardarUsuario> createGuardarUsuario(GuardarUsuario value) {
        return new JAXBElement<GuardarUsuario>(_GuardarUsuario_QNAME, GuardarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarUsuarioResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GuardarUsuarioResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "guardarUsuarioResponse")
    public JAXBElement<GuardarUsuarioResponse> createGuardarUsuarioResponse(GuardarUsuarioResponse value) {
        return new JAXBElement<GuardarUsuarioResponse>(_GuardarUsuarioResponse_QNAME, GuardarUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidarUsuario }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ValidarUsuario }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "validarUsuario")
    public JAXBElement<ValidarUsuario> createValidarUsuario(ValidarUsuario value) {
        return new JAXBElement<ValidarUsuario>(_ValidarUsuario_QNAME, ValidarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidarUsuarioResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ValidarUsuarioResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.gestorseguridadws.una.ac.cr/", name = "validarUsuarioResponse")
    public JAXBElement<ValidarUsuarioResponse> createValidarUsuarioResponse(ValidarUsuarioResponse value) {
        return new JAXBElement<ValidarUsuarioResponse>(_ValidarUsuarioResponse_QNAME, ValidarUsuarioResponse.class, null, value);
    }

}
