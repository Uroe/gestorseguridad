
package cr.ac.una.gestorseguridad.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para cambiarClave complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="cambiarClave"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="claveT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="claveN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cambiarClave", propOrder = {
    "claveT",
    "claveN"
})
public class CambiarClave {

    protected String claveT;
    protected String claveN;

    /**
     * Obtiene el valor de la propiedad claveT.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveT() {
        return claveT;
    }

    /**
     * Define el valor de la propiedad claveT.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveT(String value) {
        this.claveT = value;
    }

    /**
     * Obtiene el valor de la propiedad claveN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveN() {
        return claveN;
    }

    /**
     * Define el valor de la propiedad claveN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveN(String value) {
        this.claveN = value;
    }

}
