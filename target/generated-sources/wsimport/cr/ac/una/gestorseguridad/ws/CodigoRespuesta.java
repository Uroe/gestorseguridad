
package cr.ac.una.gestorseguridad.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para codigoRespuesta.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="codigoRespuesta"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="CORRECTO"/&amp;gt;
 *     &amp;lt;enumeration value="ERROR_ACCESO"/&amp;gt;
 *     &amp;lt;enumeration value="ERROR_PERMISOS"/&amp;gt;
 *     &amp;lt;enumeration value="ERROR_NOENCONTRADO"/&amp;gt;
 *     &amp;lt;enumeration value="ERROR_CLIENTE"/&amp;gt;
 *     &amp;lt;enumeration value="ERROR_INTERNO"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "codigoRespuesta")
@XmlEnum
public enum CodigoRespuesta {

    CORRECTO,
    ERROR_ACCESO,
    ERROR_PERMISOS,
    ERROR_NOENCONTRADO,
    ERROR_CLIENTE,
    ERROR_INTERNO;

    public String value() {
        return name();
    }

    public static CodigoRespuesta fromValue(String v) {
        return valueOf(v);
    }

}
