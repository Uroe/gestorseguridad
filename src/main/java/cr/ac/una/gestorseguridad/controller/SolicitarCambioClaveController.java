/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.service.UsuarioService;
import cr.ac.una.gestorseguridad.util.FlowController;
import cr.ac.una.gestorseguridad.util.Formato;
import cr.ac.una.gestorseguridad.util.Mensaje;
import cr.ac.una.gestorseguridad.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class SolicitarCambioClaveController extends Controller implements Initializable {

    @FXML
    private VBox root;
    @FXML
    private TextField txtCorreo;
    @FXML
    private JFXButton btnEnviar;

    Usuario usuario;
    List<Node> requeridos = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtCorreo.setTextFormatter(Formato.getInstance().maxLengthFormat(80));
        usuario = new Usuario();
        nuevoEmpleado();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtCorreo));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindActualizar() {
        txtCorreo.textProperty().bindBidirectional(usuario.usuCorreo);
    }

    private void unbindActualizar() {
        txtCorreo.textProperty().unbindBidirectional(usuario.usuCorreo);
    }

    private void nuevoEmpleado() {
        unbindActualizar();
        usuario = new Usuario();
        bindActualizar();
    }

    @Override
    public void initialize() {
    }

    @FXML
    private void enviarCorreo(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar actualización", getStage(), invalidos);
            } else {
                UsuarioService service = new UsuarioService();
                Respuesta respuesta = service.generarClaveTemp(txtCorreo.getText());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar actualización", getStage(), respuesta.getMensaje());
                } else {
                    unbindActualizar();
                    usuario = (Usuario) respuesta.getResultado("Usuario");
                    bindActualizar();
                    respuesta = service.enviarCorreoClave(usuario.getUsuCorreo(), usuario.getUsuClaveTemp(), usuario.getUsuNombre());
                    if (!respuesta.getEstado()) {
                        new Mensaje().showModal(Alert.AlertType.ERROR, "Enviar correo", getStage(), respuesta.getMensaje());
                    } else {
                        new Mensaje().showModal(Alert.AlertType.INFORMATION, "Cambio de clave", getStage(), "Correo enviado exitosamente");
                        getStage().close();
                        FlowController.getInstance().goViewInWindow("CambiarContra");
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SolicitarCambioClaveController.class.getName()).log(Level.SEVERE, "Error guardando la actualización del usuario.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar actualización", getStage(), "Ocurrio un error guardando la actualizacion del usuario.");
        }
    }

}
