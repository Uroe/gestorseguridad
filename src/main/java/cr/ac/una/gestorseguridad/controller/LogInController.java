/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.service.UsuarioService;
import cr.ac.una.gestorseguridad.util.AppContext;
import cr.ac.una.gestorseguridad.util.FlowController;
import cr.ac.una.gestorseguridad.util.Mensaje;
import cr.ac.una.gestorseguridad.util.Respuesta;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class LogInController extends Controller implements Initializable {

    @FXML
    private VBox root;
    @FXML
    private Label lblTitulo;
    @FXML
    private TextField txtUsuario;
    @FXML
    private PasswordField pwfContraseña;
    @FXML
    private JFXButton btnShow;
    @FXML
    private JFXButton btnHide;
    @FXML
    private JFXButton btnIniciarSesion;
    @FXML
    private JFXButton btnRegistrar;
    @FXML
    private Hyperlink hplOlvidoContra;
    private String password;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtUsuario.clear();
        pwfContraseña.clear();
        btnHide.setVisible(false);
        hplOlvidoContra.setOnAction(t -> {
            FlowController.getInstance().goViewInWindow("SolicitarCambioClave");
        });
    }

    @FXML
    private void onActionBtnShow(ActionEvent event) {
        password = pwfContraseña.getText();
        pwfContraseña.clear();
        pwfContraseña.setPromptText(password);
        btnShow.setVisible(false);
        btnShow.setDisable(true);
        btnHide.setVisible(true);
        btnHide.setDisable(false);
    }

    @FXML
    private void onActionBtnHide(ActionEvent event) {
        pwfContraseña.setText(password);
        pwfContraseña.setPromptText("Contraseña");
        btnHide.setVisible(false);
        btnHide.setDisable(true);
        btnShow.setVisible(true);
        btnShow.setDisable(false);
    }

    @FXML
    private void onActionBtnIniciarSesion(ActionEvent event) {
        try {
            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnIniciarSesion.getScene().getWindow(), "Es necesario digitar un usuario para ingresar al sistema.");
            } else if (pwfContraseña.getText() == null || pwfContraseña.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnIniciarSesion.getScene().getWindow(), "Es necesario digitar la clave para ingresar al sistema.");
            } else {
                UsuarioService usuarioService = new UsuarioService();
                Respuesta respuesta = usuarioService.validarUsuario(txtUsuario.getText(), pwfContraseña.getText());
                if (respuesta.getEstado()) {
                    Usuario usuarioDto = (Usuario) respuesta.getResultado("Usuario");
                    if (usuarioDto.getUsuAdministrador().equals("S")) {
                        AppContext.getInstance().set("Usuario", usuarioDto);
                        AppContext.getInstance().set("Token", usuarioDto.getToken());
                        if (getStage().getOwner() == null) {
                            FlowController.getInstance().goMain();
                        }
                        getStage().close();
                    } else {
                        new Mensaje().showModal(Alert.AlertType.ERROR, "Ingreso", getStage(), "No tiene permiso para acceder"
                                + " a esta aplicacion");
                    }

                } else {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Ingreso", getStage(),
                            "Sus credenciales no coinciden");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }
    }

    @FXML
    private void onActionBtnRegistrar(ActionEvent event) {
        FlowController.getInstance().goViewInWindow("Registro");
        FlowController.getInstance().initialize();
    }

    @Override
    public void initialize() {
    }

}
