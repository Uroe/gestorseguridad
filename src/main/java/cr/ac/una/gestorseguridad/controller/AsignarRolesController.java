package cr.ac.una.gestorseguridad.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import cr.ac.una.gestorseguridad.model.Rol;
import cr.ac.una.gestorseguridad.model.Sistema;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.service.RolService;
import cr.ac.una.gestorseguridad.service.SistemaService;
import cr.ac.una.gestorseguridad.service.UsuarioService;
import cr.ac.una.gestorseguridad.util.FlowController;
import cr.ac.una.gestorseguridad.util.Mensaje;
import cr.ac.una.gestorseguridad.util.Respuesta;
import cr.ac.una.gestorseguridad.ws.RolDto;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class AsignarRolesController extends Controller implements Initializable {

    @FXML
    private Label lblNombre;
    @FXML
    private ImageView imgFoto;
    @FXML
    private TableView<Sistema> tbvSistemas;
    @FXML
    private TableColumn<Sistema, String> tbcNomSistema;
    @FXML
    private JFXButton btnAsignarRol;
    @FXML
    private Label lblCedula;
    @FXML
    private VBox containerRoles;
    @FXML
    private SplitMenuButton smbUsuarios;
    @FXML
    private Label lblInfo;
    @FXML
    private JFXButton btnQuitarRol;

    Usuario usuarioDto;
    Sistema sistemaDto;
    Rol rolDto;
    List<Sistema> sistemas;
    List<Rol> roles;
    List<Usuario> usuarios = new ArrayList<>();
    List<JFXCheckBox> chkroles = new ArrayList<>();
    List<MenuItem> itemUsuarios = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rolDto = new Rol();
        usuarioDto = new Usuario();
        UsuarioService usuarioService = new UsuarioService();
        Respuesta resp = usuarioService.getUsuarios();
        if (resp.getEstado()) {
            usuarios = (List<Usuario>) resp.getResultado("Usuarios");
            for (Usuario usuario1 : usuarios) {
                itemUsuarios.add(new MenuItem(usuario1.getUsuNombre() + " " + usuario1.getUsuPApellido()));
            }
            smbUsuarios.getItems().addAll(itemUsuarios);
            itemUsuarios.forEach(i -> {
                i.setOnAction((t) -> {
                    for (Usuario usuario1 : usuarios) {
                        if (i.getText().equals(usuario1.getUsuNombre() + " " + usuario1.getUsuPApellido())) {
                            Respuesta r = usuarioService.getUsuario(usuario1.getUsuId());
                            usuarioDto = (Usuario) r.getResultado("Usuario");
                            cargarUsuario();
                        }
                    }
                });
            });
        }

        ////////////////////////////////////////
        SistemaService service = new SistemaService();
        Respuesta respuesta = service.getSistemas();
        if (respuesta.getEstado()) {
            sistemas = (List<Sistema>) respuesta.getResultado("Sistemas");
            tbcNomSistema.setCellValueFactory(cd -> cd.getValue().nombre);
            tbvSistemas.getItems().addAll(sistemas);
            tbvSistemas.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Sistema> observable, Sistema oldValue, Sistema newValue) -> {
                if (newValue != null) {
                    sistemaDto = newValue;
                    containerRoles.getChildren().clear();
                    chkroles.clear();
                    cargarRoles(sistemaDto.getId());
                    if (usuarioDto != null) {
                        validarRolesAsig();
                    }
                }
            });

        }
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void AsignarRol(ActionEvent event) {
        String nombre = null;
        for (JFXCheckBox chk : chkroles) {
            if (chk.isSelected()) {
                nombre = chk.getText();
            }
        }
        if (usuarioDto != null) {
            rolDto = getRolSeleccionado(nombre);
            usuarioDto.setModificado(true);
            rolDto.getUsuarios().add(usuarioDto.get());
            rolDto.setSistema(sistemaDto.get());
            RolService service = new RolService();
            Respuesta respuesta = service.guardarRol(rolDto);
            if (!respuesta.getEstado()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar rol", getStage(), "Error al guardar el"
                        + " rol");
            } else {
                rolDto = (Rol) respuesta.getResultado("Rol");
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar rol", getStage(), "Rol asignado con"
                        + " exito");
            }
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar rol", getStage(), "Aun no ha seleccionado"
                    + " un usuario");
        }
    }

    private void cargarUsuario() {
        lblInfo.setVisible(true);
        lblCedula.setText("Cedula: " + usuarioDto.getUsuCedula());
        lblNombre.setText("Nombre: " + usuarioDto.getUsuNombre());
        ByteArrayInputStream bis = new ByteArrayInputStream(usuarioDto.getUsuFoto());
        Image foto = new Image(bis);
        imgFoto.setImage(foto);
        validarRolesAsig();
    }

    private Rol getRolSeleccionado(String nombre) {
        Rol rol = roles.stream().filter((r) -> r.getNombre().equals(nombre)).findAny().get();
        return rol;
    }

    private void cargarRoles(Long id) {

        RolService rolService = new RolService();
        Respuesta r = rolService.getRoles(id);
        if (!r.getEstado()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar roles", getStage(), "Este sistema no posee "
                    + "roles");
        }
        roles = (List<Rol>) r.getResultado("Roles");
        roles.forEach(t -> {
            chkroles.add(new JFXCheckBox(t.getNombre()));
        });
        containerRoles.getChildren().addAll(chkroles);
        chkroles.forEach(c -> {
            c.getStyleClass().add("jfx-subtitulo-label2");
        });
    }

    private void validarRolesAsig() {
        if (!usuarioDto.getRoles().isEmpty()) {
            for (RolDto role : usuarioDto.getRoles()) {
                for (JFXCheckBox chkrole : chkroles) {
                    if (role.getRolNombre().equals(chkrole.getText())) {
                        RolService service = new RolService();
                        Respuesta resp = service.getRol(role.getRolId());
                        Rol rol = (Rol) resp.getResultado("Rol");
                        if (rol.getSistema().getSisId().equals(sistemaDto.getId())) {
                            chkrole.setSelected(true);
                        } else {
                            chkrole.setSelected(false);
                        }
                    }
                }
            }
        }
    }

    @FXML
    private void RegresarAlMenu(ActionEvent event) {
        FlowController.getInstance().goMain();
    }

    @FXML
    private void QuitarRol(ActionEvent event) {
        RolService service = new RolService();
        validarRolesAsig();
        String nombre = null;
        for (JFXCheckBox chk : chkroles) {
            if (chk.isSelected()) {
                nombre = chk.getText();
            }
        }
        if (usuarioDto != null) {
            rolDto = getRolSeleccionado(nombre);
            Respuesta r = service.getRol(rolDto.getId());
            rolDto = (Rol) r.getResultado("Rol");
            rolDto.setSistema(sistemaDto.get());
            rolDto.getUsuariosEliminados().add(usuarioDto.get());
            Respuesta respuesta = service.guardarRol(rolDto);
            if (!respuesta.getEstado()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar rol", getStage(), "Error al guardar el"
                        + " rol");
            } else {
                rolDto = (Rol) respuesta.getResultado("Rol");
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar rol", getStage(), "Rol eliminado con"
                        + " exito");
            }
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar rol", getStage(), "Aun no ha seleccionado"
                    + " un usuario");
        }
    }
}
