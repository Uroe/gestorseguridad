package cr.ac.una.gestorseguridad.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.service.UsuarioService;
import cr.ac.una.gestorseguridad.util.Formato;
import cr.ac.una.gestorseguridad.util.Mensaje;
import cr.ac.una.gestorseguridad.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class CambiarContraController extends Controller implements Initializable {

    @FXML
    private VBox root;
    @FXML
    private PasswordField pwfActual;
    @FXML
    private PasswordField pwfNueva;
    @FXML
    private PasswordField pwfConfirmar;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnCancelar;

    Usuario usuario;
    List<Node> requeridos = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pwfActual.setTextFormatter(Formato.getInstance().maxLengthFormat(15));
        pwfNueva.setTextFormatter(Formato.getInstance().maxLengthFormat(15));
        pwfConfirmar.setTextFormatter(Formato.getInstance().maxLengthFormat(15));
        usuario = new Usuario();
        nuevaContra();
        indicarRequeridos();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(pwfActual, pwfNueva, pwfConfirmar));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindContra(Boolean nuevo) {
        pwfActual.textProperty().bindBidirectional(usuario.usuClaveTemp);
        pwfNueva.textProperty().bindBidirectional(usuario.usuClave);
        pwfConfirmar.textProperty().bindBidirectional(usuario.usuClave);
    }

    private void unbindContra() {
        pwfActual.textProperty().unbindBidirectional(usuario.usuClaveTemp);
        pwfNueva.textProperty().unbindBidirectional(usuario.usuClave);
        pwfConfirmar.textProperty().unbindBidirectional(usuario.usuClave);
    }

    private void nuevaContra() {
        unbindContra();
        usuario = new Usuario();
        bindContra(true);
        pwfActual.clear();
        pwfActual.requestFocus();
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar contraseña", getStage(), invalidos);
            } else {

                UsuarioService service = new UsuarioService();
                Respuesta respuesta = service.cambiarClave(pwfActual.getText(), pwfNueva.getText());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar contraseña", getStage(), respuesta.getMensaje());
                } else {
                    unbindContra();
                    usuario = (Usuario) respuesta.getResultado("Usuario");
                    bindContra(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar contraseña", getStage(), "Contraseña actualizada correctamente.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CambiarContraController.class.getName()).log(Level.SEVERE, "Error guardando la contraseña.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar contraseña", getStage(), "Ocurrio un error guardando la contraseña.");
        }
    }

    @FXML
    private void onActionBtnCancelar(ActionEvent event) {
    }

    @Override
    public void initialize() {
    }

}
