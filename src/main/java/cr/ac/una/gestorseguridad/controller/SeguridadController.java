/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.service.UsuarioService;
import cr.ac.una.gestorseguridad.util.BindingUtils;
import cr.ac.una.gestorseguridad.util.FlowController;
import cr.ac.una.gestorseguridad.util.Formato;
import cr.ac.una.gestorseguridad.util.Mensaje;
import cr.ac.una.gestorseguridad.util.Respuesta;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class SeguridadController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtPapellido;
    @FXML
    private JFXTextField txtSapellido;
    @FXML
    private JFXTextField txtCedula;
    @FXML
    private JFXTextField txtTelefono;
    @FXML
    private JFXTextField txtCelular;
    @FXML
    private JFXButton btnCrgFoto;
    @FXML
    private JFXCheckBox chkActivo;
    @FXML
    private JFXTextField txtCorreo;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXTextField txtContrasena;
    @FXML
    private JFXRadioButton rdbEspanol;
    @FXML
    private ToggleGroup tggIdioma;
    @FXML
    private JFXRadioButton rdbIngles;
    @FXML
    private JFXButton btnNuevo;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXCheckBox chkAdministrador;
    @FXML
    private ImageView imvFoto;

    Usuario usuarioDto;
    List<Node> requeridos = new ArrayList<>();
    List<File> selectedFiles;
    Image fotoNueva;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtId.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtPapellido.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtSapellido.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtCedula.setTextFormatter(Formato.getInstance().cedulaFormat(40));
        txtCelular.setTextFormatter(Formato.getInstance().cedulaFormat(15));
        txtTelefono.setTextFormatter(Formato.getInstance().cedulaFormat(15));
        txtCorreo.setTextFormatter(Formato.getInstance().maxLengthFormat(80));
        txtUsuario.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtContrasena.setTextFormatter(Formato.getInstance().maxLengthFormat(8));
        rdbEspanol.setUserData("E");
        rdbIngles.setUserData("I");
        usuarioDto = new Usuario();
        nuevoRegistro();
        indicarRequeridos();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre, txtPapellido, txtSapellido, txtCedula, txtCelular, txtCorreo, txtUsuario, txtContrasena));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindRegistro(Boolean nuevo) {
        if (!nuevo) {
            txtId.textProperty().bind(usuarioDto.usuId);
        }
        txtNombre.textProperty().bindBidirectional(usuarioDto.usuNombre);
        txtPapellido.textProperty().bindBidirectional(usuarioDto.usuPApellido);
        txtSapellido.textProperty().bindBidirectional(usuarioDto.usuSApellido);
        txtCedula.textProperty().bindBidirectional(usuarioDto.usuCedula);
        txtTelefono.textProperty().bindBidirectional(usuarioDto.usuTelefono);
        txtCelular.textProperty().bindBidirectional(usuarioDto.usuCelular);
        txtCorreo.textProperty().bindBidirectional(usuarioDto.usuCorreo);
        chkAdministrador.selectedProperty().bindBidirectional(usuarioDto.usuAdministrador);
        txtUsuario.textProperty().bindBidirectional(usuarioDto.usuUsuario);
        txtContrasena.textProperty().bindBidirectional(usuarioDto.usuClave);
        chkActivo.selectedProperty().bindBidirectional(usuarioDto.usuEstado);
        BindingUtils.bindToggleGroupToProperty(tggIdioma, usuarioDto.usuIdioma);
    }

    private void unbindRegistro() {
        txtId.textProperty().unbind();
        txtNombre.textProperty().unbindBidirectional(usuarioDto.usuNombre);
        txtPapellido.textProperty().unbindBidirectional(usuarioDto.usuPApellido);
        txtSapellido.textProperty().unbindBidirectional(usuarioDto.usuSApellido);
        txtCedula.textProperty().unbindBidirectional(usuarioDto.usuCedula);
        txtTelefono.textProperty().unbindBidirectional(usuarioDto.usuTelefono);
        txtCelular.textProperty().unbindBidirectional(usuarioDto.usuCelular);
        txtCorreo.textProperty().unbindBidirectional(usuarioDto.usuCorreo);
        txtUsuario.textProperty().unbindBidirectional(usuarioDto.usuUsuario);
        txtContrasena.textProperty().unbindBidirectional(usuarioDto.usuClave);
        chkAdministrador.selectedProperty().unbindBidirectional(usuarioDto.usuAdministrador);
        chkActivo.selectedProperty().unbindBidirectional(usuarioDto.usuEstado);
        BindingUtils.unbindToggleGroupToProperty(tggIdioma, usuarioDto.usuIdioma);
    }

    private void nuevoRegistro() {
        unbindRegistro();
        usuarioDto = new Usuario();
        bindRegistro(true);
        validarAdministrador();
        txtId.clear();
        txtId.requestFocus();
        imvFoto.setVisible(false);
    }

    void validarAdministrador() {
        if (chkAdministrador.isSelected()) {
            requeridos.addAll(Arrays.asList(txtUsuario, txtContrasena));
            txtUsuario.setDisable(false);
            txtContrasena.setDisable(false);
        } else {
            requeridos.removeAll(Arrays.asList(txtUsuario, txtContrasena));
            txtUsuario.validate();
            txtContrasena.validate();
            txtUsuario.setDisable(true);
            txtContrasena.setDisable(true);
        }
    }

    @FXML
    private void onKeyPressedTxtId(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtId.getText().isEmpty()) {
            cargarUsuario(Long.valueOf(txtId.getText()));
        }
    }

    private void cargarUsuario(Long id) {
        UsuarioService service = new UsuarioService();
        Respuesta respuesta = service.getUsuario(id);

        if (respuesta.getEstado()) {
            unbindRegistro();
            usuarioDto = (Usuario) respuesta.getResultado("Usuario");
            bindRegistro(false);
            validarAdministrador();
            validarRequeridos();
            ByteArrayInputStream bis = new ByteArrayInputStream(usuarioDto.usuFoto);
            Image image = new Image(bis);
            imvFoto.setImage(image);
            imvFoto.setVisible(true);
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Usuario", getStage(), respuesta.getMensaje());
        }
    }

    @FXML
    private void onActionBtnCrgFoto(ActionEvent event) throws FileNotFoundException {
        FileChooser fileChooser = new FileChooser();
        selectedFiles = fileChooser.showOpenMultipleDialog(null);
        fotoNueva = new Image(new FileInputStream(selectedFiles.get(0)));
        imvFoto.setImage(fotoNueva);
    }

    @FXML
    private void onActionSeleccionAdministrador(ActionEvent event) {
        validarAdministrador();
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Registro", getStage(), "¿Esta seguro que desea limpiar el registro?")) {
            nuevoRegistro();
        }
    }

    @FXML
    private void onActionBtnBuscar(ActionEvent event) {

    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Usuario", getStage(), invalidos);
            } else {
                if (selectedFiles != null) {
                    InputStream in = new FileInputStream(selectedFiles.get(0));
                    usuarioDto.setUsuFoto(in.readAllBytes());
                }
                UsuarioService service = new UsuarioService();
                Respuesta respuesta = service.guardarUsuario(usuarioDto);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Usuario", getStage(), respuesta.getMensaje());
                } else {
                    unbindRegistro();
                    usuarioDto = (Usuario) respuesta.getResultado("Usuario");
                    bindRegistro(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar Usuario", getStage(), "Usuario actualizado correctamente.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SeguridadController.class.getName()).log(Level.SEVERE, "Error guardando el usuario.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Usuario", getStage(), "Ocurrio un error guardando el usuario.");
        }
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void RegresarAlMenu(ActionEvent event) {
        FlowController.getInstance().goMain();
    }
}
