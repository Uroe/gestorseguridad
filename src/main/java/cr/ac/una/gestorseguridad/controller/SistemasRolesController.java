/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Rol;
import cr.ac.una.gestorseguridad.model.Sistema;
import cr.ac.una.gestorseguridad.service.RolService;
import cr.ac.una.gestorseguridad.service.SistemaService;
import cr.ac.una.gestorseguridad.util.FlowController;
import cr.ac.una.gestorseguridad.util.Formato;
import cr.ac.una.gestorseguridad.util.Mensaje;
import cr.ac.una.gestorseguridad.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class SistemasRolesController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private TabPane tbpSisRol;
    @FXML
    private Tab tbSistemas;
    @FXML
    private JFXTextField txtIdSistema;
    @FXML
    private JFXTextField txtNombreSistema;
    @FXML
    private JFXCheckBox chkActivoSistema;
    @FXML
    private TableView<Sistema> tbvSistemas;
    @FXML
    private TableColumn<Sistema, String> tbcIdSistema;
    @FXML
    private TableColumn<Sistema, String> tbcNombreSistema;
    @FXML
    private TableColumn<Sistema, Boolean> tbcEstadoSistema;
    @FXML
    private JFXButton btnNuevoSistema;
    @FXML
    private JFXButton btnEliminarSistema;
    @FXML
    private JFXButton btnGuardarSistema;

    @FXML
    private Tab tbRoles;
    @FXML
    private JFXTextField txtIdRol;
    @FXML
    private JFXTextField txtNombreRol;
    @FXML
    private JFXCheckBox chkActivoRol;
    @FXML
    private TableView<Rol> tbvRol;
    @FXML
    private TableColumn<Rol, String> tbcIdRol;
    @FXML
    private TableColumn<Rol, String> tbcNombreRol;
    @FXML
    private TableColumn<Rol, Boolean> tbcEstadoRol;
    @FXML
    private JFXButton btnNuevoRol;
    @FXML
    private JFXButton btnEliminarRol;
    @FXML
    private JFXButton btnGuardarRol;

    private Sistema sistemaDto;

    private Rol rolDto;

    List<Node> requeridosSistema = new ArrayList<>();

    List<Node> requeridosRol = new ArrayList<>();

    SistemaService serviceSis = new SistemaService();
    List<Sistema> listaSistema = new ArrayList<>();

    RolService serviceRol = new RolService();
    List<Rol> listaRol = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtIdSistema.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombreSistema.setTextFormatter(Formato.getInstance().letrasFormat(20));
        sistemaDto = new Sistema();
        nuevoSistema();
        indicarRequeridosSistema();
        ///////////////////////////////////////////////
        txtIdRol.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombreRol.setTextFormatter(Formato.getInstance().letrasFormat(20));
        rolDto = new Rol();
        nuevoRol();
        indicarRequeridosRol();
        tablaSistemas();
    }

    public void indicarRequeridosSistema() {
        requeridosSistema.clear();
        requeridosSistema.addAll(Arrays.asList(txtNombreSistema));
    }

    public String validarRequeridosSistema() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridosSistema) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindSistema(Boolean nuevo) {
        if (!nuevo) {
            txtIdSistema.textProperty().bind(sistemaDto.id);
        }
//        sistemaDto.setNombre(txtNombreSistema.getText());
        txtNombreSistema.textProperty().bindBidirectional(sistemaDto.nombre);
        chkActivoSistema.selectedProperty().bindBidirectional(sistemaDto.estado);
    }

    private void unbindSistema() {
        txtIdSistema.textProperty().unbind();
        txtNombreSistema.textProperty().unbindBidirectional(sistemaDto.nombre);
        chkActivoSistema.selectedProperty().unbindBidirectional(sistemaDto.estado);
    }

    private void nuevoSistema() {
        unbindSistema();
        sistemaDto = new Sistema();
        bindSistema(true);
        txtIdSistema.clear();
        txtNombreSistema.clear();
        txtIdSistema.requestFocus();
    }

    public void indicarRequeridosRol() {
        requeridosRol.clear();
        requeridosRol.addAll(Arrays.asList(txtNombreRol));
    }

    public String validarRequeridosRol() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridosRol) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindRol(Boolean nuevo) {
        if (!nuevo) {
            txtIdRol.textProperty().bind(rolDto.id);
        }
        txtNombreRol.textProperty().bindBidirectional(rolDto.nombre);
        chkActivoRol.selectedProperty().bindBidirectional(rolDto.estado);
    }

    private void unbindRol() {
        txtIdRol.textProperty().unbind();
        txtNombreSistema.textProperty().unbindBidirectional(rolDto.nombre);
        chkActivoRol.selectedProperty().unbindBidirectional(rolDto.estado);
    }

    private void nuevoRol() {
        unbindRol();
        rolDto = new Rol();
        bindRol(true);
        txtIdRol.clear();
        txtNombreRol.clear();
        txtIdRol.requestFocus();
    }

    @FXML
    private void selectionChangeTabRolesSistema(Event event) {
        if (tbRoles.isSelected()) {
            if (sistemaDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Sistema", getStage(), "Debe cargar el sistema al que desea crear roles.");
                tbpSisRol.getSelectionModel().select(tbSistemas);
            }
        } else if (tbSistemas.isSelected()) {

        }
    }

    ///////////////////////////////////////////////
    @FXML
    private void onKeyPressedTxtIdSistema(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && txtIdSistema.getText() != null && !txtIdSistema.getText().isEmpty()) {
            cargarSistema(Long.valueOf(txtIdSistema.getText()));
        }
    }

    private void cargarSistema(Long id) {
        SistemaService service = new SistemaService();
        Respuesta respuesta = service.getSistema(id);

        if (respuesta.getEstado()) {
            unbindSistema();
            sistemaDto = (Sistema) respuesta.getResultado("Sistema");
            bindSistema(false);
            validarRequeridosSistema();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Sistema", getStage(), respuesta.getMensaje());
        }
    }

    @FXML
    private void onActionBtnNuevoSistema(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Sistema", getStage(), "¿Esta seguro que desea limpiar el registro?")) {
            nuevoSistema();
        }
    }

    @FXML
    private void onActionBtnEliminarSistema(ActionEvent event) {
        try {
            if (sistemaDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Sistema", getStage(), "Debe cargar el sistema que desea eliminar.");
            } else {

                SistemaService service = new SistemaService();
                Respuesta respuesta = service.eliminarSistema(sistemaDto.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Sistema", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar Sistema", getStage(), "Sistema eliminado correctamente.");
                    nuevoSistema();
                    tbvSistemas.getItems().clear();
                    tablaSistemas();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SistemasRolesController.class
                    .getName()).log(Level.SEVERE, "Error eliminando el sistema.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Sistema", getStage(), "Ocurrio un error eliminando el sistema.");
        }

    }

    @FXML
    private void onActionBtnGuardarSistema(ActionEvent event) {
        try {
            String invalidos = validarRequeridosSistema();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Sistema", getStage(), invalidos);
            } else {
                bindSistema(true);
                SistemaService service = new SistemaService();
                Respuesta respuesta = service.guardarSistema(sistemaDto);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Sistema", getStage(), respuesta.getMensaje());
                } else {
                    unbindSistema();
                    sistemaDto = (Sistema) respuesta.getResultado("Sistema");
                    bindSistema(false);
                    tbvSistemas.getItems().clear();
                    tablaSistemas();
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar Sistema", getStage(), "Sistema actualizado correctamente.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SistemasRolesController.class
                    .getName()).log(Level.SEVERE, "Error guardando el sistema.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Sistema", getStage(), "Ocurrio un error guardando el sistema.");
        }
    }

    ///////////////////////////////////////////////
    @FXML
    private void onKeyPressedTxtIdRol(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && txtIdRol.getText() != null && !txtIdRol.getText().isEmpty()) {
            cargarRol(Long.valueOf(txtIdRol.getText()));
        }
    }

    private void cargarRol(Long id) {
        RolService service = new RolService();
        Respuesta respuesta = service.getRol(id);

        if (respuesta.getEstado()) {
            unbindRol();
            rolDto = (Rol) respuesta.getResultado("Rol");
            bindRol(false);
            validarRequeridosRol();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Rol", getStage(), respuesta.getMensaje());
        }
    }

    @FXML
    private void onActionBtnNuevoRol(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Rol", getStage(), "¿Esta seguro que desea limpiar el registro?")) {
            nuevoRol();
        }
    }

    @FXML
    private void onActionBtnEliminarRol(ActionEvent event) {
        try {
            if (rolDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Rol", getStage(), "Debe cargar el rol que desea eliminar.");
            } else {

                RolService service = new RolService();
                Respuesta respuesta = service.eliminarRol(rolDto.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Rol", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar Rol", getStage(), "Rol eliminado correctamente.");
                    nuevoRol();
                    tbvRol.getItems().clear();
                    tablaRoles();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SistemasRolesController.class
                    .getName()).log(Level.SEVERE, "Error eliminando el rol.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Rol", getStage(), "Ocurrio un error eliminando el rol.");
        }
    }

    @FXML
    private void onActionBtnGuardarRol(ActionEvent event) {
        try {
            String invalidos = validarRequeridosRol();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Rol", getStage(), invalidos);
            } else {
                rolDto.setSistema(sistemaDto.get());
                RolService service = new RolService();
                Respuesta respuesta = service.guardarRol(rolDto);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Rol", getStage(), respuesta.getMensaje());
                } else {
                    unbindRol();
                    rolDto = (Rol) respuesta.getResultado("Rol");
                    bindRol(false);
                    tbvRol.getItems().clear();
                    tablaRoles();
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar Rol", getStage(), "Rol actualizado correctamente.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SistemasRolesController.class
                    .getName()).log(Level.SEVERE, "Error guardando el rol.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Rol", getStage(), "Ocurrio un error guardando el rol.");
        }
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void RegresarAlMenu(ActionEvent event) {
        FlowController.getInstance().goMain();
    }

    private void tablaSistemas() {
        Respuesta res = serviceSis.getSistemas();

        if (res.getEstado()) {
            listaSistema = (List<Sistema>) res.getResultado("Sistemas");
            tbvSistemas.getItems().addAll(listaSistema);
            tbvSistemas.refresh();

            tbcIdSistema.setCellValueFactory(cd -> cd.getValue().id);
            tbcNombreSistema.setCellValueFactory(cd -> cd.getValue().nombre);
            tbcEstadoSistema.setCellValueFactory(cd -> cd.getValue().estado);
            tbvSistemas.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Sistema> observable, Sistema oldValue, Sistema newValue) -> {
                unbindSistema();
                if (newValue != null) {
                    tbvRol.getItems().removeAll(listaRol);
                    sistemaDto = newValue;
                    bindSistema(false);
                    tablaRoles();
                }
            });
        }
    }

    private void tablaRoles() {
        Respuesta resp = serviceRol.getRoles(sistemaDto.getId());

        if (resp.getEstado()) {

            listaRol = (List<Rol>) resp.getResultado("Roles");
            tbvRol.getItems().addAll(listaRol);
            tbvRol.refresh();

            tbcIdRol.setCellValueFactory(cd -> cd.getValue().id);
            tbcNombreRol.setCellValueFactory(cd -> cd.getValue().nombre);
            tbcEstadoRol.setCellValueFactory(cd -> cd.getValue().estado);

            tbvRol.getSelectionModel()
                    .selectedItemProperty().addListener((ObservableValue<? extends Rol> observable, Rol oldValue, Rol newValue) -> {
                        unbindRol();
                        if (newValue != null) {
                            rolDto = newValue;
                            bindRol(false);
                        }
                    });
        }
    }
}
