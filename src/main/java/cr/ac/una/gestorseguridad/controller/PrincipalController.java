/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorseguridad.util.FlowController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class PrincipalController extends Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void EditarUsuarios(ActionEvent event) {
        FlowController.getInstance().goView("Seguridad");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void MantenimientoSisRol(ActionEvent event) {
        FlowController.getInstance().goView("SistemasRoles");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void AccesosUsuarios(ActionEvent event) {
        FlowController.getInstance().goView("AsignarRoles");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void CerrarSesion(ActionEvent event) {
        FlowController.getInstance().salir();
        FlowController.getInstance().goViewInWindow("LogIn");
        FlowController.getInstance().initialize();
    }

}
