package cr.ac.una.gestorseguridad.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXClippedPane;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.service.UsuarioService;
import cr.ac.una.gestorseguridad.util.BindingUtils;
import cr.ac.una.gestorseguridad.util.Formato;
import cr.ac.una.gestorseguridad.util.Mensaje;
import cr.ac.una.gestorseguridad.util.PanelCamara;
import cr.ac.una.gestorseguridad.util.Respuesta;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;

public class RegistroController extends Controller implements Initializable {

    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtCedula;
    @FXML
    private TextField txtTelefono;
    @FXML
    private TextField txtCelular;
    @FXML
    private TextField txtCorreo;
    @FXML
    private JFXClippedPane clippedPane;
    @FXML
    private PasswordField txtContraseña;
    @FXML
    private JFXButton btnClave;
    @FXML
    private RadioButton rbdEspañol;
    @FXML
    private ToggleGroup tggIdioma;
    @FXML
    private RadioButton rbdIngles;
    @FXML
    private JFXButton btnAbrirCamara;
    @FXML
    private JFXButton btnBuscarFoto;
    @FXML
    private JFXButton btnRegistrar;
    @FXML
    private TextField txtPapellido;
    @FXML
    private TextField txtSapellido;
    @FXML
    private TextField txtUsuario;

    File foto;
    String ruta = null;
    Usuario usuario;
    List<TextField> requeridos = new ArrayList<>();
    @FXML
    private ImageView imvFoto;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rbdEspañol.setUserData("E");
        rbdIngles.setUserData("F");
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtPapellido.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtSapellido.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtCedula.setTextFormatter(Formato.getInstance().cedulaFormat(40));
        txtCelular.setTextFormatter(Formato.getInstance().integerFormat());
        txtTelefono.setTextFormatter(Formato.getInstance().integerFormat());
        txtCorreo.setTextFormatter(Formato.getInstance().maxLengthFormat(60));
        txtUsuario.setTextFormatter(Formato.getInstance().maxLengthFormat(20));
        txtContraseña.setTextFormatter(Formato.getInstance().maxLengthFormat(15));
        usuario = new Usuario();
        btnClave.setVisible(false);
        indicarRequeridos();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre, txtPapellido, txtSapellido,
                txtCedula, txtCelular, txtUsuario, txtCorreo));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (TextField txt : requeridos) {
            if (txt.getText() == null || txt.getText().length() == 0) {
                if (validos) {
                    invalidos += txt.getAccessibleText();
                } else {
                    invalidos += "," + txt.getAccessibleText();
                }
                validos = false;
            }
        }
        if (txtContraseña.getText() == null || txtContraseña.getText().length() == 0) {
            if (validos) {
                invalidos += txtContraseña.getAccessibleText();
            } else {
                invalidos += "," + txtContraseña.getAccessibleText();
            }
            validos = false;
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindUsuario() throws IOException {
        usuario.setUsuNombre(txtNombre.getText());
        usuario.setUsuCedula(txtCedula.getText());
        usuario.setUsuPApellido(txtPapellido.getText());
        usuario.setUsuSApellido(txtSapellido.getText());
        usuario.setUsuCelular(txtCelular.getText());
        usuario.setUsuTelefono(txtTelefono.getText());
        usuario.setUsuCorreo(txtCorreo.getText());
        usuario.setUsuUsuario(txtUsuario.getText());
        usuario.setUsuClave(txtContraseña.getText());
        if (ruta != null) {
            cargarFoto();
        }
        BindingUtils.bindToggleGroupToProperty(tggIdioma, usuario.usuIdioma);
    }

    private void unbindUsuario() {
        txtCedula.clear();
        txtCelular.clear();
        txtContraseña.clear();
        txtCorreo.clear();
        txtNombre.clear();
        txtPapellido.clear();
        txtSapellido.clear();
        txtTelefono.clear();
        txtUsuario.clear();
        if (ruta != null) {
            foto.delete();
        }

        BindingUtils.unbindToggleGroupToProperty(tggIdioma, usuario.usuIdioma);
    }

    @FXML
    private void onKeyTypedtxtContraseña(KeyEvent event) {
        if (txtContraseña.getText().isEmpty()) {
            btnClave.setVisible(false);
        } else {
            btnClave.setVisible(true);
        }
    }

    @FXML
    private void onMouseReleasedbtnClave(MouseEvent event) {
        clippedPane.getChildren().remove(txtContraseña);
        clippedPane.getChildren().add(txtContraseña);
        clippedPane.getChildren().remove(btnClave);
        clippedPane.getChildren().add(btnClave);
    }

    @FXML
    private void onMousePressedbtnClave(MouseEvent event) {
        TextField clave = new TextField(txtContraseña.getText());
        clippedPane.getChildren().add(clave);
        clippedPane.getChildren().remove(btnClave);
        clippedPane.getChildren().add(btnClave);
    }

    @FXML
    private void onActionbtnAbrirCamara(ActionEvent event) {
        PanelCamara camara = new PanelCamara();
        camara.setBounds(300, 400, 400, 300);
        camara.setVisible(true);
        ruta = "";
    }

    private void cargarFoto() throws IOException {
        foto = new File("Foto_Usuario.png");
        InputStream in = new FileInputStream(foto);
        usuario.setUsuFoto(in.readAllBytes());
    }

    @FXML
    private void onActionbtnBuscarFoto(ActionEvent event) throws FileNotFoundException, IOException {
        FileChooser fileChooser = new FileChooser();
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);
        InputStream in = new FileInputStream(selectedFiles.get(0));
        usuario.setUsuFoto(in.readAllBytes());
    }

    @FXML
    private void onActionbtnRegistrar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Registrar usuario", getStage(), invalidos);
            } else {
                bindUsuario();
                UsuarioService service = new UsuarioService();
                Respuesta respuesta = service.guardarUsuario(usuario);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Registrar usuario", getStage(), respuesta.getMensaje());
                } else {
                    unbindUsuario();
                    Usuario usuarioDto = (Usuario) respuesta.getResultado("Usuario");
                    if (usuarioDto.getUsuId() == Long.valueOf(1)) {
                        respuesta = service.asignarAdministrador(usuarioDto.getUsuId());
                        usuarioDto = (Usuario) respuesta.getResultado("Usuario");
                    }
                    ruta = null;
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Registrar usuario", getStage(), "Usuario registrado correctamente.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(RegistroController.class
                    .getName()).log(Level.SEVERE, "Error registrando el usuario.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Registrar usuario", getStage(), "Ocurrio un error registrando el usuario.");
        }
    }

    @Override
    public void initialize() {

    }

}
