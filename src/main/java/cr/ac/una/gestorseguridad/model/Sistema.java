/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.model;

import cr.ac.una.gestorseguridad.ws.RolDto;
import cr.ac.una.gestorseguridad.ws.SistemaDto;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

/**
 *
 * @author JosueNG
 */
public class Sistema {

    public SimpleStringProperty id;
    public SimpleStringProperty nombre;
    public SimpleBooleanProperty estado;
    private String token;
    private Boolean modificado;
    private List<RolDto> roles;

    public Sistema() {
        this.id = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.estado = new SimpleBooleanProperty(true);
        this.modificado = false;
    }

    public Sistema(SistemaDto sistemaDto) {
        this();
        this.id.set(sistemaDto.getSisId().toString());
        this.nombre.set(sistemaDto.getSisNombre());
        this.estado.set(sistemaDto.getSisEstado().equalsIgnoreCase("A"));
    }

    public SistemaDto get() {
        SistemaDto s = new SistemaDto();
        s.setSisId(this.getId());
        s.setSisNombre(this.getNombre());
        s.setSisEstado(this.getEstado());
        return s;
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getEstado() {
        return estado.getValue() ? "A" : "I";
    }

    public void setEstado(String estado) {
        this.estado.setValue(estado.equalsIgnoreCase("A"));
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public List<RolDto> getRoles() {
        return roles;
    }

    public void setRoles(List<RolDto> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "SistemaDto{" + "id=" + id + ", nombre=" + nombre + ", estado=" + estado + '}';
    }

}
