/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.model;

import cr.ac.una.gestorseguridad.ws.RolDto;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

/**
 *
 * @author Ale
 */
public class Usuario {

    public SimpleStringProperty usuId;
    public SimpleStringProperty usuNombre;
    public SimpleStringProperty usuPApellido;
    public SimpleStringProperty usuSApellido;
    public SimpleStringProperty usuCedula;
    public SimpleStringProperty usuCorreo;
    public SimpleStringProperty usuTelefono;
    public SimpleStringProperty usuCelular;
    public SimpleStringProperty usuUsuario;
    public SimpleStringProperty usuClave;
    public SimpleStringProperty usuClaveTemp;
    public SimpleBooleanProperty usuAdministrador;
    public ObjectProperty<String> usuIdioma;
    public SimpleBooleanProperty usuEstado;
    public byte[] usuFoto;
    public List<RolDto> roles;

    private String token;
    private Boolean modificado;

    public Usuario() {
        this.modificado = false;
        this.usuId = new SimpleStringProperty();
        this.usuNombre = new SimpleStringProperty();
        this.usuPApellido = new SimpleStringProperty();
        this.usuSApellido = new SimpleStringProperty();
        this.usuCedula = new SimpleStringProperty();
        this.usuCorreo = new SimpleStringProperty();
        this.usuTelefono = new SimpleStringProperty();
        this.usuCelular = new SimpleStringProperty();
        this.usuUsuario = new SimpleStringProperty();
        this.usuClave = new SimpleStringProperty();
        this.usuClaveTemp = new SimpleStringProperty();
        this.usuAdministrador = new SimpleBooleanProperty(false);
        this.usuIdioma = new SimpleObjectProperty("E");
        this.usuEstado = new SimpleBooleanProperty(false);
        this.usuFoto = new byte[100];
        this.roles = FXCollections.observableArrayList();
    }

    public Usuario(UsuarioDto usuarioDto) {
        this();
        this.modificado = usuarioDto.isModificado();
        this.usuId.set(usuarioDto.getUsuId().toString());
        this.usuNombre.set(usuarioDto.getUsuNombre());
        this.usuPApellido.set(usuarioDto.getUsuPapellido());
        this.usuSApellido.set(usuarioDto.getUsuSapellido());
        this.usuCedula.set(usuarioDto.getUsuCedula());
        this.usuCorreo.set(usuarioDto.getUsuCorreo());
        this.usuTelefono.set(usuarioDto.getUsuTelefono());
        this.usuCelular.set(usuarioDto.getUsuCelular());
        this.usuUsuario.set(usuarioDto.getUsuUsuario());
        this.usuClave.set(usuarioDto.getUsuClave());
        this.usuClaveTemp.set(usuarioDto.getUsuClaveTemp());
        this.usuAdministrador.set(usuarioDto.getUsuAdministrador().equalsIgnoreCase("S"));
        this.usuIdioma.set(usuarioDto.getUsuIdioma());
        this.usuEstado.set(usuarioDto.getUsuEstado().equalsIgnoreCase("A"));
        this.usuFoto = usuarioDto.getUsuFoto();
        this.token = usuarioDto.getToken();
        this.roles = usuarioDto.getRoles();
    }

    public UsuarioDto get() {
        UsuarioDto u = new UsuarioDto();
        u.setModificado(this.getModificado());
        u.setUsuId(this.getUsuId());
        u.setUsuNombre(this.getUsuNombre());
        u.setUsuPapellido(this.getUsuPApellido());
        u.setUsuSapellido(this.getUsuSApellido());
        u.setUsuCedula(this.getUsuCedula());
        u.setUsuCorreo(this.getUsuCorreo());
        u.setUsuTelefono(this.getUsuTelefono());
        u.setUsuCelular(this.getUsuCelular());
        u.setUsuUsuario(this.getUsuUsuario());
        u.setUsuClave(this.getUsuClave());
        u.setUsuClaveTemp(this.getUsuClaveTemp());
        u.setUsuAdministrador(this.getUsuAdministrador());
        u.setUsuIdioma(this.getUsuIdioma());
        u.setUsuEstado(this.getUsuEstado());
        u.setUsuFoto(this.getUsuFoto());
        u.setToken(this.getToken());
        u.getRoles().addAll(this.getRoles());
        return u;
    }

    public Long getUsuId() {
        if (usuId.get() != null && !usuId.get().isEmpty()) {
            return Long.valueOf(usuId.get());
        } else {
            return null;
        }
    }

    public void setUsuId(Long usuId) {
        this.usuId.set(usuId.toString());
    }

    public String getUsuNombre() {
        return usuNombre.get();
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre.set(usuNombre.toString());
    }

    public String getUsuPApellido() {
        return usuPApellido.get();
    }

    public void setUsuPApellido(String usuPApellido) {
        this.usuPApellido.set(usuPApellido.toString());
    }

    public String getUsuSApellido() {
        return usuSApellido.get();
    }

    public void setUsuSApellido(String usuSApellido) {
        this.usuSApellido.set(usuSApellido.toString());
    }

    public String getUsuCedula() {
        return usuCedula.get();
    }

    public void setUsuCedula(String usuCedula) {
        this.usuCedula.set(usuCedula.toString());
    }

    public String getUsuCorreo() {
        return usuCorreo.get();
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo.set(usuCorreo.toString());
    }

    public String getUsuTelefono() {
        return usuTelefono.get();
    }

    public void setUsuTelefono(String usuTelefono) {
        this.usuTelefono.set(usuTelefono.toString());
    }

    public String getUsuCelular() {
        return usuCelular.get();
    }

    public void setUsuCelular(String usuCelular) {
        this.usuCelular.set(usuCelular.toString());
    }

    public String getUsuUsuario() {
        return usuUsuario.get();
    }

    public void setUsuUsuario(String usuUsuario) {
        this.usuUsuario.set(usuUsuario.toString());
    }

    public String getUsuClave() {
        return usuClave.get();
    }

    public void setUsuClave(String usuClave) {
        this.usuClave.set(usuClave.toString());
    }
    
        public String getUsuClaveTemp() {
        return usuClaveTemp.get();
    }

    public void setUsuClaveTemp(String usuClave) {
        this.usuClaveTemp.set(usuClave.toString());
    }

    public String getUsuAdministrador() {
        return usuAdministrador.getValue() ? "S" : "N";
    }

    public void setUsuAdministrador(String usuAdministrador) {
        this.usuAdministrador.setValue(usuAdministrador.equalsIgnoreCase("S"));
    }

    public String getUsuIdioma() {
        return usuIdioma.get();
    }

    public void setUsuIdioma(String usuIdioma) {
        this.usuIdioma.set(usuIdioma);
    }

    public String getUsuEstado() {
        return usuEstado.getValue() ? "A" : "I";
    }

    public void setUsuEstado(String usuEstado) {
        this.usuEstado.setValue(usuEstado.equalsIgnoreCase("A"));
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public byte[] getUsuFoto() {
        return usuFoto;
    }

    public void setUsuFoto(byte[] usuFoto) {
        this.usuFoto = usuFoto;
    }

    public List<RolDto> getRoles() {
        return roles;
    }

    public void setRoles(List<RolDto> roles) {
        this.roles = roles;
    }
    
    @Override
    public String toString() {
        return "UsuarioDto{" + "usuId=" + usuId + ", usuNombre=" + usuNombre + ", usuPApellido=" + usuPApellido + ", usuSApellido=" + usuSApellido + ", usuCedula=" + usuCedula + ", usuCorreo=" + usuCorreo + ", usuTelefono=" + usuTelefono + ", usuCelular=" + usuCelular + ", usuUsuario=" + usuUsuario + ", usuClave=" + usuClave + ", usuAdministrador=" + usuAdministrador + ", usuIdioma=" + usuIdioma + ", usuEstado=" + usuEstado + ", token=" + token + ", modificado=" + modificado + '}';
    }

}
