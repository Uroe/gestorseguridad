/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.model;

import cr.ac.una.gestorseguridad.ws.RolDto;
import cr.ac.una.gestorseguridad.ws.SistemaDto;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

/**
 *
 * @author JosueNG
 */
public class Rol {

    public SimpleStringProperty id;
    public SimpleStringProperty nombre;
    public SimpleBooleanProperty estado;
    private String token;
    private Boolean modificado;
    private List<UsuarioDto> usuarios;
    private List<UsuarioDto> usuariosEliminados;
    private SistemaDto sistema;

    public Rol() {
        this.id = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.estado = new SimpleBooleanProperty(true);
        usuarios = FXCollections.observableArrayList();
        usuariosEliminados = new ArrayList<>();
        sistema = new SistemaDto();
    }

    public Rol(RolDto rolDto) {
        this();
        this.id.set(rolDto.getRolId().toString());
        this.nombre.set(rolDto.getRolNombre());
        this.estado.set(rolDto.getRolEstado().equalsIgnoreCase("A"));
        this.usuarios = rolDto.getUsuarios();
        this.usuariosEliminados = rolDto.getUsuariosEliminados();
        this.sistema = rolDto.getSistema();
    }

    public RolDto get() {
        RolDto r = new RolDto();
        r.setRolId(this.getId());
        r.setRolNombre(this.getNombre());
        r.setRolEstado(this.getEstado());
        r.getUsuarios().addAll(this.getUsuarios());
        r.getUsuariosEliminados().addAll(this.getUsuariosEliminados());
        r.setSistema(this.getSistema());
        return r;
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getEstado() {
        return estado.getValue() ? "A" : "I";
    }

    public void setEstado(String estado) {
        this.estado.setValue(estado.equalsIgnoreCase("A"));
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public List<UsuarioDto> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioDto> usuarios) {
        this.usuarios = usuarios;
    }

    public SistemaDto getSistema() {
        return sistema;
    }

    public void setSistema(SistemaDto sistema) {
        this.sistema = sistema;
    }

    public List<UsuarioDto> getUsuariosEliminados() {
        return usuariosEliminados;
    }

    public void setUsuariosEliminados(List<UsuarioDto> usuariosEliminados) {
        this.usuariosEliminados = usuariosEliminados;
    }

    @Override
    public String toString() {
        return "SistemaDto{" + "id=" + id + ", nombre=" + nombre + ", estado=" + estado + '}';
    }

}
