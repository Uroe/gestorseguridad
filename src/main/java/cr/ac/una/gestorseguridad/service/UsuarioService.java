/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.service;

import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.util.Request;
import cr.ac.una.gestorseguridad.util.Respuesta;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class UsuarioService {

    GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
    GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();

    public Respuesta validarUsuario(String usuario, String clave) {
        try {
            UsuarioDto usuarioDto = gestor.validarUsuario(usuario, clave);
            if (usuarioDto == null) {
                return new Respuesta(false, "Usuario no encontrado", "validarUsuario");
            }
            return new Respuesta(true, "", "", "Usuario", new Usuario(usuarioDto));
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo el usuario [" + usuario + "]", ex);
            return new Respuesta(false, "Error obteniendo el usuario.", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta getUsuario(Long id) {
        try {
            UsuarioDto usuario = gestor.getUsuario(id);
            if (usuario == null) {
                return new Respuesta(false, "Usuario no encontrado", "getUsuario");
            }
            return new Respuesta(true, "", "", "Usuario", new Usuario(usuario));
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo el usuario [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el usuario.", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta getUsuarios() {
        try {
            List<UsuarioDto> usuarioDtos = gestor.getUsuarios();
            List<Usuario> usuarios = new ArrayList<>();
            for (UsuarioDto usuarioDto : usuarioDtos) {
                usuarios.add(new Usuario(usuarioDto));
            }
            if (usuarios.isEmpty()) {
                return new Respuesta(false, "", "");
            }
            return new Respuesta(true, "", "", "Usuarios", usuarios);
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo usuarios.", ex);
            return new Respuesta(false, "Error obteniendo usuarios.", "getUsuarios " + ex.getMessage());
        }
    }

    public Respuesta guardarUsuario(Usuario usuario) {
        try {
            UsuarioDto usuarioDto = gestor.guardarUsuario(usuario.get());
            if (usuarioDto == null) {
                return new Respuesta(false, "Error al guardar el usuario", "guardarUsuario");
            }
            return new Respuesta(true, "", "", "Usuario", new Usuario(usuarioDto));
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error guardando el usuario.", ex);
            return new Respuesta(false, "Error guardando el usuario.", "guardarUsuario " + ex.getMessage());
        }
    }

    public Respuesta asignarAdministrador(Long id) {
        try {
            UsuarioDto usuarioDto = gestor.asignarAdministrador(id);
            if (usuarioDto == null) {
                return new Respuesta(false, "Error al asignar", "asignarAdministrador");
            }
            return new Respuesta(true, "", "", "Usuario", new Usuario(usuarioDto));
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error asignando el administrador.", ex);
            return new Respuesta(false, "Error asignando el administrador.", "asignarAdministrador " + ex.getMessage());
        }
    }

    public Respuesta cambiarClave(String claveT, String claveN) {
        try {
            UsuarioDto usuarioDto = gestor.cambiarClave(claveT, claveN);
            if (usuarioDto == null) {
                return new Respuesta(false, "Error al cambiar la clave", "cambiarClave");
            }
            return new Respuesta(true, "", "", "Usuario", new Usuario(usuarioDto));
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error cambiando la clave.", ex);
            return new Respuesta(false, "Error cambiando la clave.", "cambiarClave " + ex.getMessage());
        }
    }

    public Respuesta generarClaveTemp(String correo) {
        try {
            UsuarioDto usuarioDto = gestor.generarClaveTemp(correo);
            if (usuarioDto == null) {
                return new Respuesta(false, "Error generando la clave temporal", "generarClaveTemp");
            }
            return new Respuesta(true, "", "", "Usuario", new Usuario(usuarioDto));
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error generando la clave temporal.", ex);
            return new Respuesta(false, "Error generando la clave temporal.", "generarClaveTemp" + ex.getMessage());
        }
    }

    public Respuesta eliminarUsuario(Long id) {
        try {
            gestor.eliminarUsuario(id);
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error eliminando el usuario.", ex);
            return new Respuesta(false, "Error eliminando el usuario.", "eliminarUsuario " + ex.getMessage());
        }
    }

    public Respuesta enviarCorreoClave(String para, String claveTemp, String usuario) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("para", para);
            parametros.put("claveTemp", claveTemp);
            parametros.put("usuario", usuario);
            Request request = new Request("CorreoController/correoclave", "/{para}/{claveTemp}/{usuario}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error enviando el correo", ex);
            return new Respuesta(false, "Error enviando el correo.", "enviarCorreo " + ex.getMessage());
        }
    }

}
