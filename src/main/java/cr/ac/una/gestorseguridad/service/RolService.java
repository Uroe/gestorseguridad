/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.service;

import cr.ac.una.gestorseguridad.model.Rol;
import cr.ac.una.gestorseguridad.util.Respuesta;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.RolDto;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class RolService {

    GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
    GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();

    public Respuesta getRol(Long id) {
        try {
            RolDto rol = gestor.getRol(id);
            if (rol == null) {
                return new Respuesta(false, "Rol no encontrado", "getRol");
            }
            return new Respuesta(true, "", "", "Rol", new Rol(rol));
        } catch (Exception ex) {
            Logger.getLogger(RolService.class.getName()).log(Level.SEVERE, "Error obteniendo el rol [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el rol.", "getRol " + ex.getMessage());
        }
    }

    public Respuesta getRoles(Long id) {
        try {
            List<RolDto> rolesDto = gestor.getRoles(id);
            List<Rol> roles = new ArrayList<>();
            for (RolDto rolDto : rolesDto) {
                roles.add(new Rol(rolDto));
            }
            if(roles.isEmpty()){
                return new Respuesta(false, "", "");
            }
            return new Respuesta(true, "", "", "Roles", roles);
        } catch (Exception ex) {
            Logger.getLogger(RolService.class.getName()).log(Level.SEVERE, "Error obteniendo roles.", ex);
            return new Respuesta(false, "Error obteniendo roles.", "getRoles " + ex.getMessage());
        }
    }

    public Respuesta guardarRol(Rol rol) {
        try {
            RolDto rolDto = gestor.guardarRol(rol.get());
            if(rolDto == null){
                return new Respuesta(false, "Error al guardar el rol", "guardarRol");
            }
            return new Respuesta(true, "", "", "Rol", new Rol(rolDto));
        } catch (Exception ex) {
            Logger.getLogger(RolService.class.getName()).log(Level.SEVERE, "Error guardando el rol.", ex);
            return new Respuesta(false, "Error guardando el rol.", "guardarRol " + ex.getMessage());
        }
    }

    public Respuesta eliminarRol(Long id) {
        try {
            gestor.eliminarRol(id);
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(RolService.class.getName()).log(Level.SEVERE, "Error eliminando el rol.", ex);
            return new Respuesta(false, "Error eliminando el rol.", "eliminarRol " + ex.getMessage());
        }
    }
}
