/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridad.service;

import cr.ac.una.gestorseguridad.model.Sistema;
import cr.ac.una.gestorseguridad.util.Respuesta;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.SistemaDto;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class SistemaService {

    GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
    GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();

    public Respuesta getSistema(Long id) {
        try {
            SistemaDto sistema = gestor.getSistema(id);
            if (sistema == null) {
                return new Respuesta(false, "Sistema no encontrado", "getSistema");
            }
            return new Respuesta(true, "", "", "Sistema", new Sistema(sistema));
        } catch (Exception ex) {
            Logger.getLogger(SistemaService.class.getName()).log(Level.SEVERE, "Error obteniendo el sistema [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el sistema.", "getSistema " + ex.getMessage());
        }
    }

    public Respuesta getSistemas() {
        try {
            List<SistemaDto> sistemas = gestor.getSistemas();
            List<Sistema> sis = new ArrayList<>();
            for (SistemaDto sistemaDto : sistemas) {
                sis.add(new Sistema(sistemaDto));
            }
            if (sis.isEmpty()) {
                return new Respuesta(false, "", "");
            }
            return new Respuesta(true, "", "", "Sistemas", sis);
        } catch (Exception ex) {
            Logger.getLogger(SistemaService.class.getName()).log(Level.SEVERE, "Error obteniendo sistemas.", ex);
            return new Respuesta(false, "Error obteniendo sistemas.", "getSistemas " + ex.getMessage());
        }
    }

    public Respuesta guardarSistema(Sistema sistema) {
        try {
            SistemaDto sistemaDto = gestor.guardarSistema(sistema.get());
            if (sistemaDto == null) {
                return new Respuesta(false, "Error guardando el sistema", "guardarSistema");
            }
            return new Respuesta(true, "", "", "Sistema", new Sistema(sistemaDto));
        } catch (Exception ex) {
            Logger.getLogger(SistemaService.class.getName()).log(Level.SEVERE, "Error guardando el sistema.", ex);
            return new Respuesta(false, "Error guardando el sistema.", "guardarSistema " + ex.getMessage());
        }
    }

    public Respuesta eliminarSistema(Long id) {
        try {
            gestor.eliminarSistema(id);
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(SistemaService.class.getName()).log(Level.SEVERE, "Error eliminando el sistema.", ex);
            return new Respuesta(false, "Error eliminando el sistema.", "eliminarSistema " + ex.getMessage());
        }
    }
}
