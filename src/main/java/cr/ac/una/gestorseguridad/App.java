package cr.ac.una.gestorseguridad;

import cr.ac.una.gestorseguridad.util.FlowController;
import javafx.application.Application;
import javafx.stage.Stage;
import java.io.IOException;
import javafx.scene.image.Image;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        FlowController.getInstance().InitializeFlow(stage, null);
        stage.getIcons().add(new Image(App.class.getResourceAsStream("/cr/ac/una/gestorseguridad/"
                + "resources/iconos/lock.png")));
        stage.setTitle("Gestor de Seguridad");
        FlowController.getInstance().goViewInWindow("LogIn");
    }

    public static void main(String[] args) {
        launch();
    }
}
