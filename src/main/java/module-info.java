module cr.ac.una.gestorseguridad {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    requires java.ws.rs;
    requires java.xml.bind;
    requires java.sql;
    requires java.base;
    requires com.jfoenix;
    requires com.sun.xml.bind;
    requires jakarta.activation;
    requires org.jvnet.staxex;
    requires java.desktop;
    requires webcam.capture;
    requires java.xml.ws;
    requires java.jws;

    opens cr.ac.una.gestorseguridad.controller to javafx.fxml, javafx.controls, com.jfoenix;
    opens cr.ac.una.gestorseguridad.ws to com.sun.xml.bind;
    opens cr.ac.una.gestorseguridad to webcam.capture;
    exports cr.ac.una.gestorseguridad to javafx.graphics;
    exports cr.ac.una.gestorseguridad.ws;
    exports cr.ac.una.gestorseguridad.model;

}
